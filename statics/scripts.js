$(document).ready( async () => {
  
  // load the base dataset
  const serverUrl = new URL("https://digital.inha.fr/richelieu/code/proj_leaflet/");
  const d = await $.ajax({
    url: new URL("statics/data/proj_leaflet.json", serverUrl),
    dataType: "json",
    mimeType: "application/json",
    crossOrigin: true
  });
  
  // initialize the map
  const m = L.map("map", {
    center: [ 48.8687452, 2.3363674 ]
    , maxBounds: L.latLngBounds([
      { lat: 48.8856701621242, lng: 2.3092982353700506 }
      , { lat: 48.829997780023035, lng: 2.3845843750075915 }
    ])
    , inertia: false  // inertia does weird things with the geojson layer
    , maxBoundsViscosity: 1.0
    , scrollWheelZoom: false
    , zoomControl: true
    , minZoom: 13  // 11 for paris + region, 13 for the neighbourhood 
    , maxZoom: 21
    , zoom: 14.7
  });
  m.createPane("bg");
  m.getPane("bg").style.zIndex="0";
  m.createPane("vector");
  m.getPane("vector").style.zIndex="1";
  m.createPane("raster");
  m.getPane("raster").style.zIndex="2";
  
  // bg tiles
  L.tileLayer(
    "https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      maxZoom: 20,
      attribution: '© OpenStreetMap'
    }, pane="bg"
  ).addTo(m);
  const vasserot = L.tileLayer(
    "https://tile.maps.huma-num.fr/uc2usU/d/Alpage_Vasserot_1830/{z}/{x}/{y}.png", {
      opacity: 0.85,
      maxZoom: 18
    }, pane="bg"
  ).addTo(m);
  
  // add geojsons to `m`
  var vectorGroup = L.layerGroup();
  for ( let i=0; i<Object.keys(d).length; i++ ) {
    var uuid = Object.keys(d)[i];
    if ( d[uuid].vector !== "" && d[uuid].isfullstreet !== true ) {
      vectorGroup.addLayer(L.geoJSON(
        d[uuid].vector,
        style={
          color: "var(--cs-blue)",
          stroke: "#ffffff",
          fillOpacity: 0.5,
          opacity: 1
        }, pane="vector"
      ));
    } else if ( d[uuid].vector !== "" && d[uuid].isfullstreet ) {
      var fullStreet = L.geoJSON(
        d[uuid].vector,
        style={
          color: "var(--cs-duck)",
          stroke: "#ffffff",
          fillOpacity: 0.5,
          opacity: 1
        }, pane="vector"
      )
    }
  }
  vectorGroup.addTo(m);
  fullStreet.addTo(m);

  // add raster files to `m`
  var rasterGroup = L.layerGroup();
  for ( let i=0; i<Object.keys(d).length; i++ ) {
    uuid = Object.keys(d)[i]
    if ( d[uuid].raster !== "" && d[uuid].latlngbounds !== false ) { 
      var x = L.imageOverlay(
        new URL(`statics/data/raster/${ d[uuid].raster }`, serverUrl),
        d[uuid].latlngbounds
      );
      rasterGroup.addLayer(x);
    }
  }
  // bug here: latlngbounds is undefined
  rasterGroup.addTo(m);
  
  // controls
  L.control.layers(
    {}, undefined, { hideSingleBase:true, position:"bottomright" }   
  ).addOverlay(vectorGroup, "vector: parcelles individuelles")
   .addOverlay(fullStreet, "vector: rue entière")
   .addOverlay(rasterGroup, "raster")
   .addOverlay(vasserot, "carte Alpage-Vasserot")
   .addTo(m);
})





