# test de projection Leaflet

ce micro-dépôt est un test de la moulinette de reprojection développée dans 
[`1_data_preparation`](https://gitlab.inha.fr/snr/rich.data/data-preparation) 
sur Leaflet.

le résultat est visible à [cette addresse](https://digital.inha.fr/richelieu/code/proj_leaflet/).

## utilisation

si les fichiers *raster* et *vecteur* sont disponibles sur le serveur de l'INHA, il 
suffit d'ouvrir le fichier `index.html` dans un navigateur pour voir le résultat de 
la reprojection.


