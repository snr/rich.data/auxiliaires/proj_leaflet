import json
import ast
import csv
import sys
import os

ds = {}

# convert csv to json
with open(os.path.join( os.path.abspath(""), "statics","data", "proj_leaflet.csv" )) as fh:
  r = csv.reader(fh, delimiter="\t", quotechar='"')
  hdr = next(r)
  # print("\n".join(f"{i}: {hdr[i]}" for i in range(len(hdr))))
  for row in r:
    row[-2] = json.loads(row[-2]) if len(row[-2]) > 0 else False        # retype geojson string to dict
    row[-1] = ast.literal_eval(row[-1]) if len(row[-1]) > 0 else False  # retype latlngbounds to list
    row[11] = ast.literal_eval(row[11])                                 # retype isfullstreet to bool
    ds[row[0]] = dict(zip( hdr[1:], row[1:] ))

# write to file
with open(os.path.join( os.path.abspath(""), "statics", "data", "proj_leaflet.json" ), mode="w") as fh:
  json.dump(ds, fh, indent=2)



